package com.dmytro_glynskyi.streamcontentprovider

import android.os.ParcelFileDescriptor
import java.io.InputStream
import java.io.OutputStream

object ParcelFileDescriptorUtil {

    fun pipeTo(outputStream: OutputStream): ParcelFileDescriptor {
        val pipe = ParcelFileDescriptor.createPipe()
        val (readSide, writeSide) = pipe.slice(0..1)
        TransferThread(ParcelFileDescriptor.AutoCloseInputStream(readSide), outputStream).start()
        return writeSide
    }

    internal class TransferThread(
        private val inputStream: InputStream,
        private val outputStream: OutputStream
    ) : Thread(TransferThread::class.java.simpleName) {

        init {
            isDaemon = true
        }

        override fun run() {
            inputStream.use {
                outputStream.use {
                    inputStream.copyTo(outputStream)
                }
            }
        }
    }
}